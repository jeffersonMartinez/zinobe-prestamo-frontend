import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/user/services/user/user.service';

@Component({
  selector: 'app-base-capital',
  templateUrl: './base-capital.component.html',
  styleUrls: ['./base-capital.component.css']
})
export class BaseCapitalComponent implements OnInit {

  remainCapital: number;

  constructor(private userService: UserService) { 
    this.remainCapital = environment.baseCapital;
  }

  ngOnInit() {
    this.userService.getTotal()
    .subscribe(total => {
      console.log(total);
      this.remainCapital = environment.baseCapital - total;
    })
  }

}
