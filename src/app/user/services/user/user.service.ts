import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';
import { environment } from 'src/environments/environment';
import { map, finalize, catchError } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ResponseGeneric } from '../../models/response';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { Constants } from 'src/app/shared/Utils/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = `${environment.endpoint}user`;
  totalObs: Subject<number>;
  userList: User[];

  constructor(private http: HttpClient, private utilService: UtilsService) {
    this.totalObs = new Subject();
   }

  /**
   * request http for get all users
   */
  getUsers() {
    this.utilService.showSpinner();
    return this.http.get<ResponseGeneric<User[]>>(this.url)
    .pipe(
      map((data: any) => {
        if (data.ok && data.user) {
          return data.user;
        } else {          
          throw Constants.ERROR_GET_DATA;
        }
      }),
      finalize(() => this.utilService.hideSpinner()),
      catchError(err => {
        console.log(err);
        if (err === Constants.ERROR_GET_DATA) {
          this.utilService.showWarningToast(Constants.ERROR_GET_DATA, Constants.ERROR_RESPOSE);
        } else {
          this.utilService.showErrorToast(Constants.ERROR_GET_DATA, Constants.ERROR_SERVER);
        }        
        throw err;
      }),
    )
  }

  /**
   * Get user by documentId
   * @param documentId 
   */
  getUserByDocumentId(documentId: string) {
    this.utilService.showSpinner();
    return this.http.get<ResponseGeneric<User>>(`${this.url}/${documentId}`)
    .pipe(
      map((data: any) => {
        if (data.ok) {
          return data.user;
        } else {          
          throw Constants.ERROR_GET_DATA;
        }
      }),
      finalize(() => this.utilService.hideSpinner()),
      catchError(err => {
        console.log(err);
        if (err === Constants.ERROR_GET_DATA) {
          this.utilService.showWarningToast(Constants.ERROR_GET_DATA, Constants.ERROR_RESPOSE);
        } else {
          this.utilService.showErrorToast(Constants.ERROR_GET_DATA, Constants.ERROR_SERVER);
        }        
        throw err;
      }),
    )
  }

  /**
   * request http for save user
   * @param user 
   */
  saveUser(user: User) {
    this.utilService.showSpinner();
    return this.http.post<ResponseGeneric<User>>(this.url, user)
    .pipe(
      map((data: any) => {
        if (data.ok && data.user) {
          this.utilService.showSuccessToast(Constants.USER_CREATE_SUCCESS);
          return data.user;
        } else {          
          throw Constants.USER_CREATE_ERROR;
        }
      }),
      finalize(() => this.utilService.hideSpinner()),
      catchError(err => {
        console.log(err);
        if (err === Constants.USER_CREATE_ERROR) {
          this.utilService.showWarningToast(Constants.USER_CREATE_ERROR, Constants.ERROR_RESPOSE);
        } else {
          this.utilService.showErrorToast(Constants.USER_CREATE_ERROR, Constants.ERROR_SERVER);
        }        
        throw err;
      }),
    )
  }
  
  /**
   * call react event for update total
   * @param total 
   */
  setTotal(total: number) {
    this.totalObs.next(total);
  }

  /**
   * return total as observable for subscriptions
   */
  getTotal() {
    return this.totalObs.asObservable();
  }

  /**
   * save user list locally
   * @param userList
   */
  setUserList(userList: [User]) {
    this.userList = userList;
  }
  
  /**
   * get user list local
   */
  getUserList() {
    return this.userList;
  }

}
