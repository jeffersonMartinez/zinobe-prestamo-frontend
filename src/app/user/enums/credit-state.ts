export enum CreditState {
    APROBADOR = 'Aprobado',
    RECHAZADO = 'Rechazado'
}