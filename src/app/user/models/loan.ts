import { CreditState } from '../enums/credit-state';
import { CreditPay } from '../enums/credit-pay';

export interface Loan {
    requestedValue: number;
    payDate: Date;
    creditState: CreditState,
    creditIsPay: CreditPay
};
