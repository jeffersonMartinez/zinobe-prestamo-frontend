import { Loan } from './loan';

export interface User {
    id: string;
    name: string;
    email: string;
    documentId: string;
    loan: [Loan];
}
