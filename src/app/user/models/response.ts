export interface ResponseGeneric<T> {
    ok: boolean;
    data: T;
}
