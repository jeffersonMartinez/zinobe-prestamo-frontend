import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../../models/user';
import { CreditState } from '../../enums/credit-state';
import { CreditPay } from '../../enums/credit-pay';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from 'src/app/shared/services/utils.service';
import { Constants } from 'src/app/shared/Utils/constants';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  creditStateArray: {option: CreditState, value: CreditState}[] = [
    { option: CreditState.APROBADOR, value: CreditState.APROBADOR },
    { option: CreditState.RECHAZADO, value: CreditState.RECHAZADO }
  ];

  creditPayArray: { option: CreditPay, value: CreditPay }[] = [
    { option: CreditPay.SI, value: CreditPay.SI },
    { option: CreditPay.NO, value: CreditPay.NO }
  ];

  canHaveCredit = true;

  // alias forms //
  get requestedValueField() {
    return this.userForm.get('loan').get('requestedValue');
  }

  get creditStateField() {
    return this.userForm.get('loan').get('creditState');
  }

  get documentIdField() {
    return this.userForm.get('documentId');
  }

  get emailField() {
    return this.userForm.get('email')
  }

  get nameField() {
    return this.userForm.get('name')
  }
  userForm: FormGroup;

  constructor(
    private userService: UserService,
    private utilsService: UtilsService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<UserCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User) {}

  ngOnInit() { 
    this.configForms();    
  }

  /**
   * config form
   */
  configForms() {
    this.userForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      documentId: ['', Validators.required],
      loan: this.fb.group({
        requestedValue: ['', Validators.required],
        payDate: [''],
        creditState: ['', Validators.required],
        creditIsPay: ['', Validators.required],
      })
    });
  }

  /**
   * update requestValue when slider change
   * @param requestAmount 
   */
  requestAmountEvent(requestAmount: number) {
    this.requestedValueField.setValue(requestAmount);    
  }

  /**
   * close modal
   */
  cancel(): void {
    this.dialogRef.close();
  }

  /**
   * pass user to parent and close modal
   */
  save() {
    if(!this.creditStateField.value && this.nameField.value) {
      this.creditStateField.setValue(this.generateRandomCreditState());
    }

    if (this.userForm.valid) {
      const user: User = this.userForm.getRawValue();
      this.saveUser(user);
    } else {
      this.utilsService.showErrorToast(Constants.USER_FORM_INVALID);
    }
  }

  /**
   * generate credit state random
   */
  generateRandomCreditState() {
    const random = Math.floor(Math.random() * 10);
    const index = random % 2;    
    return this.creditStateArray[index].value;
  }

  saveUser(user: User) {
    this.userService.saveUser(user)
    .subscribe(res => this.dialogRef.close(res));
  }

  check(event) {    
    const documentId = event.target.value;
    if(documentId) {
      this.userService.getUserByDocumentId(documentId)
      .subscribe(res => {
        console.log(res); 
        if(res) {
          this.nameField.setValue(res.name);
          this.documentIdField.setValue(res.documentId);
          this.emailField.setValue(res.email);
          this.userCanHaveCredit(res);
        } else {
          this.nameField.setValue('');
          this.emailField.setValue('');
          this.canHaveCredit = true;
        }
      });
    }
  }

  userCanHaveCredit(user: User) {
    this.canHaveCredit = true;
    user.loan.forEach(l => {
      if (l.creditState === this.creditStateArray[1].value) { // cannot have credit - reject
        this.canHaveCredit = false;
        this.creditStateField.setValue(this.creditStateArray[1].value);
      } else {        
        this.creditStateField.setValue(this.creditStateArray[0].value);
      }
      return;
    });
  }

}
