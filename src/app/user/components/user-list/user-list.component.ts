import { Component, OnInit, ViewChild } from '@angular/core';

import { UserService } from '../../services/user/user.service';
import { User } from '../../models/user';

import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { UserCreateComponent } from '../user-create/user-create.component';
import { CreditRecordComponent } from '../credit-record/credit-record.component';
import { TypeTable } from '../../enums/type-table';
import { LoanRecordsComponent } from 'src/app/loan/components/loan-records/loan-records.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[];

  displayedColumns: string[] = ['Nombre', 'Correo', 'Cédula', 'DETALLE'];
  dataSource: MatTableDataSource<User[]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  
  constructor(private userService: UserService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.initDataSource();
    this.getListUsers();
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource([]);
  }

  setupTable() {    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * get all users
   */
  getListUsers() {
    this.userService.getUsers()
    .subscribe(users => {
      this.users = users;
      console.log(users);   
      this.dataSource = new MatTableDataSource(users);
      this.setupTable();
      this.calculateTotalLoan();
    });
  }

  /**
   * calculates the remaining total of the base capital
   */
  calculateTotalLoan() {
    let total = 0;
    this.users.forEach((u: User) => {
      u.loan.forEach((l: any) => {
        if (l.requestedValue) {
          total += l.requestedValue;
        }
      });
    });
    this.userService.setTotal(total);
  }

  /**
   * filter table
   * @param filterValue 
   */
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * open modal for create user
   */
  openDialog() {
    const dialogRef = this.dialog.open(UserCreateComponent, {
      width: '98%'
    });

    dialogRef.afterClosed()
    .subscribe(result => {
      if(result) {
        this.getListUsers();
      }
    });
  }

  /**
   * open modal for see record credit
   * @param user 
   */
  showRecord(user: User = undefined) {
    const dialogRef = this.dialog.open(CreditRecordComponent, {
      width: '70%',
      data: { loan: user.loan, name: user.name, documentId: user.documentId }
    });

    dialogRef.afterClosed();
  }
  
  showLoanRecord(type: TypeTable) {
    const dialogRef = this.dialog.open(LoanRecordsComponent, {
      width: '70%',
      data: { type }
    });

    dialogRef.afterClosed();
  }
}
