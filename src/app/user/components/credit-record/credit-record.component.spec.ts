import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditRecordComponent } from './credit-record.component';

describe('CreditRecordComponent', () => {
  let component: CreditRecordComponent;
  let fixture: ComponentFixture<CreditRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
