import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Loan } from '../../models/loan';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-credit-record',
  templateUrl: './credit-record.component.html',
  styleUrls: ['./credit-record.component.css']
})
export class CreditRecordComponent implements OnInit {

  
  displayedColumns: string[] = ['Id', 'Valor Solicitado', 'Credito Pagado', 'Estado Crédito'];
  dataSource: MatTableDataSource<Loan>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userName: string;
  documentId: string;

  constructor(
    public dialogRef: MatDialogRef<CreditRecordComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.loadDataUser();
    this.loadTable();
  }

  loadTable() {
    this.dataSource = new MatTableDataSource(this.data.loan);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;    
  }

  loadDataUser() {
    this.userName = this.data.name;
    this.documentId = this.data.documentId;
  }
  /**
   * close modal
   */
  cancel(): void {
    this.dialogRef.close();
  }  

}
