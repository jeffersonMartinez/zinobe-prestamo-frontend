import { Component, OnInit, ViewChild, Inject, AfterContentChecked, AfterViewInit, AfterViewChecked } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/user/models/user';
import { UserService } from 'src/app/user/services/user/user.service';
import { Loan } from 'src/app/user/models/loan';
import { CreditState } from 'src/app/user/enums/credit-state';

@Component({
  selector: 'app-loan-records',
  templateUrl: './loan-records.component.html',
  styleUrls: ['./loan-records.component.css']
})
export class LoanRecordsComponent implements OnInit {

  displayedColumns: string[] = ['Id', 'Valor Solicitado', 'Credito Pagado', 'Estado Crédito'];
  dataSource: MatTableDataSource<Loan>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userList: User[] = [];
  loanList: Loan[] = [];

  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<LoanRecordsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.loadDataUser();  
    }, 100) 
  }

  loadTable() {
    this.dataSource = new MatTableDataSource(this.loanList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;    
  }

  loadDataUser() {
    this.userService.getUsers()
    .subscribe(res => {
      this.userList = res;
      console.log(this.userList);
    
      this.userList.forEach(u => {
        u.loan.forEach(l => {
          if(l.creditState === CreditState.RECHAZADO) {
            this.loanList.push(l);
          }
        });
      });
      this.loadTable();
    });
  }

  /**
   * close modal
   */
  cancel(): void {
    this.dialogRef.close();
  }  

}
