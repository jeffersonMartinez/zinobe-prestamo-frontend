import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { env } from 'process';

@Component({
  selector: 'app-loan-create',
  templateUrl: './loan-create.component.html',
  styleUrls: ['./loan-create.component.css']
})
export class LoanCreateComponent implements OnInit {

  minAmout = environment.minAmount;
  maxAmout = environment.maxAmount;
  value: number = 0;
  @Output() requestAmount = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  cancel() {

  }

  save() {
    
  }

  blur() {
    this.requestAmount.emit(this.value);
  }

}
