import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoanCreateComponent } from './components/loan-create/loan-create.component';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { LoanRecordsComponent } from './components/loan-records/loan-records.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [LoanCreateComponent, LoanRecordsComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSliderModule,
    SharedModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    NgxSpinnerModule,
  ],
  exports: [
    LoanCreateComponent,
    LoanRecordsComponent
  ],
  entryComponents: [
    LoanCreateComponent,
    LoanRecordsComponent 
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LoanModule { }
