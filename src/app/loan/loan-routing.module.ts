import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoanCreateComponent } from './components/loan-create/loan-create.component';
import { LoanRecordsComponent } from './components/loan-records/loan-records.component';


const routes: Routes = [
  { path: '', component: LoanCreateComponent },
  { path: 'record', component: LoanRecordsComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
