export class Constants {
    // user module
    static USER_CREATE_SUCCESS = 'Usuario creado exitosamente';
    static USER_CREATE_ERROR = 'Ocurrió un error al intentar crear guardar el usuario, por favor intenta nuevamente';
    static USER_FORM_INVALID = 'Existen campos que faltan por diligenciar';
    static ERROR_GET_DATA = 'Ocurrió un error al obtener los resultados, por favor intenta nuevamente';
    
    // error code request services
    static ERROR_SERVER = '500';
    static ERROR_RESPOSE = '400';
}
