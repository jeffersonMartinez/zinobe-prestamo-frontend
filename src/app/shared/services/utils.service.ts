import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  showSuccessToast(message: string) {
    this.toastr.success(message);
  }

  showErrorToast(message: string, title: string = undefined) {
    this.toastr.error(message, title);
  }

  showWarningToast(message: string, title: string = undefined) {
    this.toastr.warning(message, title);
  }

  showSpinner() {
    this.spinner.show();
  }

  hideSpinner() {
    this.spinner.hide();
  }

}
