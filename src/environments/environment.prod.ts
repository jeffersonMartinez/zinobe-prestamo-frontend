export const environment = {
  production: true,
  baseCapital: 100000000,
  minAmount: 10000,
  maxAmount: 10000000,
  endpoint: 'http://localhost:3000/api/'
};
